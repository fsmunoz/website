<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Free software timeline, and the memory of things past (interlaye.red)</title>

<meta name="description" content="Free software timeline, and the memory of things past (interlaye.red)">
<meta name="keywords" content="Free software timeline, and the memory of things past (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Posts.html" rel="up" title="Posts">
<link href="Embedded-Development-with-uLisp.html" rel="next" title="Embedded Development with uLisp">
<link href="Leaving-IBM.html" rel="prev" title="Leaving IBM">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Free-Software-Timeline.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Free-Software-Timeline">
<div class="nav-panel">
<p>
Next: <a href="Embedded-Development-with-uLisp.html" accesskey="n" rel="next">Interactive embedded development with uLisp, Arduino and Emacs</a>, Previous: <a href="Leaving-IBM.html" accesskey="p" rel="prev">Farewell IBM!</a>, Up: <a href="Posts.html" accesskey="u" rel="up">Posts</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Free-software-timeline_002c-and-the-memory-of-things-past"><span>Free software timeline, and the memory of things past<a class="copiable-link" href="#Free-software-timeline_002c-and-the-memory-of-things-past"> &para;</a></span></h3>
<div id="_article_date_div">
<i class="i" id="_article_date">2021-06-01</i>
</div>
<a class="index-entry-id" id="index-2021-06"></a>
<a class="index-entry-id" id="index-Presentation"></a>
<a class="index-entry-id" id="index-Free-Software-2"></a>

<p>While writing this timeline I was surprised by how recent and at the
same time distant it feels, especially the events in the late 90s.
</p>
<p>I&rsquo;m quite proud of this presentation, not only for allowing me to set
help set the record straight in a number of topics, but because it was
a modest way of stressing how all the discussion on &ldquo;open source&rdquo;
today has a context, and a history behind it.
</p>
<img class="image" src="images/free_software_timeline.jpg" alt="images/free_software_timeline">

<dl class="table">
<dt><em class="dfn">1976</em></dt>
<dd><ul class="itemize mark-bullet">
<li>First release of Emacs
</li></ul>

</dd>
<dt><em class="dfn">1982</em></dt>
<dd><ul class="itemize mark-bullet">
<li>Donal Knuth releases TeX
</li></ul>

</dd>
<dt><em class="dfn">1983</em></dt>
<dd><ul class="itemize mark-bullet">
<li>&ldquo;Free Unix!&rdquo;: Richard Stallman (RMS) announces the GNU Project.
</li></ul>

</dd>
<dt><em class="dfn">1985</em></dt>
<dd><ul class="itemize mark-bullet">
<li>The GNU Manifesto is published: software freedom and the goal of a free OS defined as goals.
</li><li>The Free Software Foundation (FSF) is incorporated with RMS as president.
</li></ul>


</dd>
<dt><em class="dfn">1987</em></dt>
<dd><ul class="itemize mark-bullet">
<li>First release of GCC.
</li><li>First release of Perl.
</li><li>Andrew S. Tanenbaum releases Minix, a free-to-use Unix clone with source code available (modification and redistribution not allowed).
</li></ul>

</dd>
<dt><em class="dfn">1989</em></dt>
<dd><ul class="itemize mark-bullet">
<li>Official publication of the GNU General Public License (GPL)
</li></ul>

</dd>
<dt><em class="dfn">1991</em></dt>
<dd><ul class="itemize mark-bullet">
<li>Linux Torvalds announces first version of the Linux kernel, with <code class="command">bash</code> and <code class="command">gcc</code> already ported.
</li><li>Keith Bostic releases BSD Net/2.
</li><li>Guido can Rossum releases Python.
</li></ul>


</dd>
<dt><em class="dfn">1993</em></dt>
<dd><ul class="itemize mark-bullet">
<li>Debian GNU/Linux first release.
</li><li>Slackware Linux first release.
</li></ul>

</dd>
<dt><em class="dfn">1993</em></dt>
<dd><ul class="itemize mark-bullet">
<li>Red Hat Linux first release.
</li></ul>

</dd>
<dt><em class="dfn">2004</em></dt>
<dd><ul class="itemize mark-bullet">
<li>Ubuntu Linux released.
</li></ul>

</dd>
</dl>





<p>... and from ~2000 onwards we see Google&rsquo;s Borg, Omega and Kubernetes
emerging, with all the ripple effect it had on cloud native (the
timeline has more, and even what it has misses a lot).
</p>
<p>Going back to the presentations I did, I was always particularly proud
of this one: the history of free software and open-source being
supported by big companies tends to be shortsighted in that it values
recent trends to the detriment of what happened to get us there.
</p></div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
