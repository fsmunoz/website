<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Emacs Lisp wmii controller (interlaye.red)</title>

<meta name="description" content="Emacs Lisp wmii controller (interlaye.red)">
<meta name="keywords" content="Emacs Lisp wmii controller (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Projects.html" rel="up" title="Projects">
<link href="cl_002drebonoise.html" rel="next" title="cl-rebonoise">
<link href="Voting-Analysis.html" rel="prev" title="Voting Analysis">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Emacs-Lisp-wmii.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Emacs-Lisp-wmii">
<div class="nav-panel">
<p>
Next: <a href="cl_002drebonoise.html" accesskey="n" rel="next">Common Lisp terrain generator</a>, Previous: <a href="Voting-Analysis.html" accesskey="p" rel="prev">Voting Analysis</a>, Up: <a href="Projects.html" accesskey="u" rel="up">Projects</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Emacs-Lisp-wmii-controller"><span>Emacs Lisp wmii controller<a class="copiable-link" href="#Emacs-Lisp-wmii-controller"> &para;</a></span></h3>

<p><code class="code">wmii-el</code> (<a class="url" href="https://github.com/fsmunoz/wmii-el">GitHub repository)</a> allows complete
control of the wmii window manager through Emacs Lisp.
</p>
<div class="float">
<img class="image" src="images/wmii-el-logo.png" alt="images/wmii-el-logo">
</div>
<p><a class="url" href="https://github.com/0intro/wmii">Wmii</a> is a tiling window manager
for X11, strongly inspired by Plan 9. For me, it was always the tiling
window manager whose behaviour I found easier, and the extensibility
of it through the Plan 9 filesystem made it scriptable from the start
&ndash; the default installation used a shell script, so that was very much
a part of the design. Some of the reasons I preferred it to
alternatives:
</p>
<ul class="itemize mark-bullet">
<li>The dynamic tilling model of wmii hits the sweetspot between being restricted to some predefined layouts and allowing all sorts of partitioning
</li><li>The way it is configured is something I personally find attractive: via reading and writing to files, using the 9P protocol. This makes it easy to integrate with everything.
</li><li>While “minimal” by most standards it comes with enough functionality to do without external applications (including some useful tools like wimenu)
</li><li>It draws titlebars in windows. For some this is uneeded, for me it makes it aesthetically pleasing in managed mode and essential in unmanaged (floating) mode
</li><li>The code itself is compact and has, with minor upkeep, withstood the passage of years.
</li></ul>

<p>I don&rsquo;t currently use it, mostly because of multi-monitor support
issues that made it difficult for me to connect to external projectors
for presenting, but it was likely the best experience in terms of
being in control of the desktop (and yes, I know about i3, which I
also used, and then sway, but they lack a 9P interface and the tilling
model is not the same).
</p>
<p>One of the reasons this worked so well was because it created
primitives, in Emacs Lisp, for the wmii functions, which made it a joy
to use Emacs to automate everything else. This example from my config
file shows how <code class="code">rcirc</code> notifications are sent to the notification
bar, and how every action received by wmii (which could be from any
source) calls a hook.
</p>
<div class="example user-lisp">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span>(<span style="color: #008000">defun</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">wmii-rcirc-notification</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">proc</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">sender</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">response</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">target</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">text</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">wmii-write-to-button-with-timer</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;rbar&quot;</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;!notice&quot;</span><span style="color: #bbbbbb"> </span>(<span style="color: #008000">format</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;%s: %s&quot;</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">sender</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">text</span>)<span style="color: #bbbbbb"> </span><span style="color: #666666">10</span>))

(<span style="color: #19177C">add-hook</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">&#39;rcirc-print-hooks</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">&#39;wmii-rcirc-notification</span>)

<span style="color: #3D7B7B; font-style: italic">;; Adding event hooks Every event received by wmii is passed to the</span>
<span style="color: #3D7B7B; font-style: italic">;; functions in wmii-event-hook; we can take advantage of that to add</span>
<span style="color: #3D7B7B; font-style: italic">;; all sorts of additional functions.</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Here we turn on transparency for every created client; this could</span>
<span style="color: #3D7B7B; font-style: italic">;; be changed to limit it to only certain clients, etc.</span>

(<span style="color: #19177C">add-hook</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">&#39;wmii-event-hook</span><span style="color: #bbbbbb"> </span>(<span style="color: #008000; font-weight: bold">lambda</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">ev</span>)
<span style="color: #bbbbbb">			     </span>(<span style="color: #008000; font-weight: bold">let</span><span style="color: #bbbbbb"> </span>((<span style="color: #19177C">event-type</span><span style="color: #bbbbbb"> </span>(<span style="color: #008000">car</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">split-string</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">ev</span>)))
<span style="color: #bbbbbb">				   </span>(<span style="color: #19177C">client</span><span style="color: #bbbbbb"> </span>(<span style="color: #008000">cadr</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">split-string</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">ev</span>))))
<span style="color: #bbbbbb">			       </span>(<span style="color: #008000">when</span><span style="color: #bbbbbb"> </span>(<span style="color: #008000">equal</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">downcase</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">event-type</span>)<span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;createclient&quot;</span>)
<span style="color: #bbbbbb">				 </span>(<span style="color: #19177C">wmii-transparency-toggle</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">client</span>)))))
</pre></div>
</pre></div>

</div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
