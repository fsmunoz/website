<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Interactive embedded development with uLisp, Arduino and Emacs (interlaye.red)</title>

<meta name="description" content="Interactive embedded development with uLisp, Arduino and Emacs (interlaye.red)">
<meta name="keywords" content="Interactive embedded development with uLisp, Arduino and Emacs (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Posts.html" rel="up" title="Posts">
<link href="Arduino-Programming-with-Ada.html" rel="next" title="Arduino Programming with Ada">
<link href="Free-Software-Timeline.html" rel="prev" title="Free Software Timeline">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Embedded-Development-with-uLisp.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Embedded-Development-with-uLisp">
<div class="nav-panel">
<p>
Next: <a href="Arduino-Programming-with-Ada.html" accesskey="n" rel="next">On the Ada language and embedded development</a>, Previous: <a href="Free-Software-Timeline.html" accesskey="p" rel="prev">Free software timeline, and the memory of things past</a>, Up: <a href="Posts.html" accesskey="u" rel="up">Posts</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Interactive-embedded-development-with-uLisp_002c-Arduino-and-Emacs"><span>Interactive embedded development with uLisp, Arduino and Emacs<a class="copiable-link" href="#Interactive-embedded-development-with-uLisp_002c-Arduino-and-Emacs"> &para;</a></span></h3>
<div id="_article_date_div">
<i class="i" id="_article_date">2017-08-04</i>
</div>
<a class="index-entry-id" id="index-2017-08"></a>
<a class="index-entry-id" id="index-Arduino"></a>
<a class="index-entry-id" id="index-Emacs"></a>
<a class="index-entry-id" id="index-Lisp"></a>

<p><small class="sc">AS I MENTIONED IN MY <a class="ref" href="Arduino-Programming-with-Ada.html">&rsquo;POST ABOUT
ADA&rsquo;</a>, LISP HAS A COMPLETELY DIFFERENT APPROACH</small> in terms of... well,
just about everything, beginning with the type system. One important
feature of Lisp environments is the high degree if interaction they
provide: Lisp Systems allowed for interaction with every part of the
system (OS, applications, window manager...) and to a smaller degree
this is what the read-eval-print-loop approach (REPL) provides in,
say, Emacs with SLIME.
</p>
<p>One interesting language I found recently was uLisp, a Lisp language for
the Arduino and MSP430 platforms. It is based on a subset of Common Lisp
and provides a different approach to development: it installs an
interpreter which is programmed via the serial interface, thus allowing
for a much more interactive development style (at a certain cost in
terms of available space).
</p>
<p>The interaction is normally made with the Arduino IDE, via the Serial
Monitor; uLisp contains a minimal editor as well, but using Emacs makes
more sense and it&rsquo;s actually easy to configure to that end; before going
into the details this is the end result (zoom in the image if necessary
to start the animation).
</p>
<img class="image" src="images/test.png" alt="&rsquo;Emacs and Arduino&rsquo;">

<p>The main thing to consider is that inferior-lisp-mode is actually based
on comint-mode, which simplifies things: we just need a way to replace
the buffer created by calling inferior-lisp with a buffer that
communicates via the serial port. This is, in turn, something which can
be done using Emacs term-mode.
</p>
<p>Combining all of this the following Emacs Lisp code will connect to the
Arduino via the serial port, rename the resulting buffer and change to
line mode.
</p>
<div class="example user-elisp">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span><span style="color: #3D7B7B; font-style: italic">;; Arduino LED blink</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Based on the example at http://www.ulisp.com/show?1LG8</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; 2017, Frederico Munoz &lt;fsmunoz@     sdf.org&gt;</span>


(<span style="color: #008000">defun</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">x</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">d</span>)
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Slowly increases and decreases the blinking interval in a loop&quot;</span>
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">pinmode</span><span style="color: #bbbbbb"> </span><span style="color: #666666">13</span><span style="color: #bbbbbb"> </span><span style="color: #880000">t</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">digitalwrite</span><span style="color: #bbbbbb"> </span><span style="color: #666666">13</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">delay</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #008000; font-weight: bold">cond</span>
<span style="color: #bbbbbb">   </span>((<span style="color: #0000FF">&lt;</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">100</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">not</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">+</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>)<span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>))
<span style="color: #bbbbbb">   </span>((<span style="color: #0000FF">&gt;</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">1000</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">not</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">-</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>)<span style="color: #bbbbbb"> </span><span style="color: #666666">-50</span>))
<span style="color: #bbbbbb">   </span>(<span style="color: #880000">t</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">not</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">+</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">d</span>)<span style="color: #bbbbbb"> </span><span style="color: #19177C">d</span>))))

(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span><span style="color: #880000">t</span><span style="color: #bbbbbb"> </span><span style="color: #666666">500</span><span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>)
</pre></div>
</pre></div>

<p><a class="uref" href="https://gist.github.com/fsmunoz/ef4a04c5f4eb117087a923d2555563af#file-arduino-serial-el">Gist</a>
</p>
<p>Creating a new Lisp file (like test.lisp) will by default enter
lisp-mode; in this mode we can use C-x C-e to send the s-expression to
the uLisp interpreter and get the result, as seen in the previous
screencast
</p>
<div class="example user-elisp">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span><span style="color: #3D7B7B; font-style: italic">;; Arduino LED blink</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Based on the example at http://www.ulisp.com/show?1LG8</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; 2017, Frederico Munoz &lt;fsmunoz@sdf.org&gt;</span>


(<span style="color: #008000">defun</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">x</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">d</span>)
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Slowly increases and decreases the blinking interval in a loop&quot;</span>
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">pinmode</span><span style="color: #bbbbbb"> </span><span style="color: #666666">13</span><span style="color: #bbbbbb"> </span><span style="color: #880000">t</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">digitalwrite</span><span style="color: #bbbbbb"> </span><span style="color: #666666">13</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #19177C">delay</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #008000; font-weight: bold">cond</span>
<span style="color: #bbbbbb">   </span>((<span style="color: #0000FF">&lt;</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">100</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">not</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">+</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>)<span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>))
<span style="color: #bbbbbb">   </span>((<span style="color: #0000FF">&gt;</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">1000</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">not</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">-</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>)<span style="color: #bbbbbb"> </span><span style="color: #666666">-50</span>))
<span style="color: #bbbbbb">   </span>(<span style="color: #880000">t</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span>(<span style="color: #19177C">not</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">x</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">+</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">s</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">d</span>)<span style="color: #bbbbbb"> </span><span style="color: #19177C">d</span>))))

(<span style="color: #19177C">b</span><span style="color: #bbbbbb"> </span><span style="color: #880000">t</span><span style="color: #bbbbbb"> </span><span style="color: #666666">500</span><span style="color: #bbbbbb"> </span><span style="color: #666666">50</span>)
</pre></div>
</pre></div>

<p><a class="url" href="https://gist.github.com/fsmunoz/424b052e1848cdda3bee043386bdc7bd#file-blink-lisp">I&rsquo;ve put the Lisp code on Github as
well</a>)
</p></div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
