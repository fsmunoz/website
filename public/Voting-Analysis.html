<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Voting Analysis (interlaye.red)</title>

<meta name="description" content="Voting Analysis (interlaye.red)">
<meta name="keywords" content="Voting Analysis (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Projects.html" rel="up" title="Projects">
<link href="Emacs-Lisp-wmii.html" rel="next" title="Emacs Lisp wmii">
<link href="Texiblog.html" rel="prev" title="Texiblog">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Voting-Analysis.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Voting-Analysis">
<div class="nav-panel">
<p>
Next: <a href="Emacs-Lisp-wmii.html" accesskey="n" rel="next">Emacs Lisp wmii controller</a>, Previous: <a href="Texiblog.html" accesskey="p" rel="prev">Texiblog</a>, Up: <a href="Projects.html" accesskey="u" rel="up">Projects</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Voting-Analysis-1"><span>Voting Analysis<a class="copiable-link" href="#Voting-Analysis-1"> &para;</a></span></h3>

<div class="float">
<img class="image" src="images/European_Parliament_33_0.png" alt="images/European_Parliament_33_0">
<div class="caption"><p>MDS plot of the distance between EU political groups.</p></div></div>
<p>I&rsquo;ve made some decently complex analysis &ndash; with the challenge of
making the end result as easily understandable and actionable as
possible &ndash; on how parties vote and what does that voting tells us
about their relative position in the ideological
spectrum. <a class="url" href="https://fsmunoz.github.io/parlamento/html/intro.html">I&rsquo;ve shared the result using Jupyter Book here</a> (in
Portuguese), and it has a smaller (in the sense that the explanations
are terser, but it&rsquo;s actually the same) section on how the same
approach applies to
<a class="url" href="https://fsmunoz.github.io/parlamento/html/European_Parliament.html">European
Parliament political groups</a>, so perhaps start there!
</p>

<div class="subsection-level-extent" id="Background">
<h4 class="subsection"><span>Background<a class="copiable-link" href="#Background"> &para;</a></span></h4>
<p>What does the voting record &ndash; and only that &ndash; tells us about the
proximity and positioning of political parties? Considering that
political activity, in the form of proposals and support/rejection of
them in voting sessions, is (in theory) the way that the platform of
each party gets expressed, what can we infer if looking exclusively at
that record?
</p>
<p>This analysis started when a discussion about the attributed seats of
recently elected parties to the Portuguese parliament caught on,
understandably because sitting more to the left or to the right is a
reflection of how the parties are perceived, even those that
ostensibly refuse such classifications.
</p><div class="float">
<img class="image" src="images/Estatesgeneral.jpg" alt="images/Estatesgeneral">
<div class="caption"><p><a class="url" href="https://commons.wikimedia.org/wiki/File:Estatesgeneral.jpg">Estates Generale in 1789</a></p></div></div><p>This is not new: the ideological proximity between different political
parties is a hotly debated topic, with the absolute and relative
positioning between them being part of the social debate. The physical
seating location of those elected by parties in a national parliament
is something that, since the 1789 National Assembly in France, carries
substantial weight in itself, and the left/right divide (today also
morphed into multiple compasses, quadrants, and other explanatory
frameworks) continues to be an important source of political
discussion.
</p>
<p>Approaches to improve on the reliance on self-identification (often
incompatible with existing majority perception, or simply inconsistent
when considered as a whole) have used different methods, most of them
dependent on the <em class="emph">a priori</em> classification of themes,
expressions, language patterns, or positions, which are then used as
qualifying criteria to determine the political positions of parties
according with their voting record, often selecting a subset of votes
determined to be particularly representative.
</p>
</div>
<div class="subsection-level-extent" id="A-new-approach">
<h4 class="subsection"><span>A new approach<a class="copiable-link" href="#A-new-approach"> &para;</a></span></h4>
<div class="float">
<img class="image" src="images/l13_32_0.png" alt="images/l13_32_0">
<div class="caption"><p>Clustermap based on computed distances.</p></div></div>
<p>The increased availability of open data allows the application of
different analytical approaches to what is a privileged source of
social expression: voting roll calls. We present an unsupervised
method of determining pairwise distances between parties based
exclusively on their voting behaviour, visualised through
clustermaps. Furthermore, we explore how clustering algorithms
(DBSCAN, Spectral Clustering) can be applied to determine grouping,
and how dimension reduction through Multidimensional Scaling (MDS)
provides an intuitive and automatable approach to visualise relative
positioning through time, without the need to depend on the subjective
classification or selection of proposals.
</p>

<p>The results of this approach to the Portuguese national parliament and
the European Parliament are presented, identifying that the tendency
of most parties to position themselves at the centre of the political
spectrum is challenged by the proposed visualisation of the voting
record. The application of this method to multi-party parliamentary
systems is discussed, and how technology and open data can play a role
in the active political involvement of citizens.
</p>

</div>
</div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
