<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Publishing messages with MQTT, using Clojure and the Eclipse Paho client (interlaye.red)</title>

<meta name="description" content="Publishing messages with MQTT, using Clojure and the Eclipse Paho client (interlaye.red)">
<meta name="keywords" content="Publishing messages with MQTT, using Clojure and the Eclipse Paho client (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Posts.html" rel="up" title="Posts">
<link href="Oracle_002dCDN.html" rel="next" title="Oracle-CDN">
<link href="Common-Lisp-and-MQTT-with-ABCL.html" rel="prev" title="Common Lisp and MQTT with ABCL">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Clojure_002dMQTT.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Clojure_002dMQTT">
<div class="nav-panel">
<p>
Next: <a href="Oracle_002dCDN.html" accesskey="n" rel="next">Oracle Data Change Notification in Clojure</a>, Previous: <a href="Common-Lisp-and-MQTT-with-ABCL.html" accesskey="p" rel="prev">Using MQTT in Common Lisp with ABCL and the Eclipse Paho client.</a>, Up: <a href="Posts.html" accesskey="u" rel="up">Posts</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Publishing-messages-with-MQTT_002c-using-Clojure-and-the-Eclipse-Paho-client"><span>Publishing messages with MQTT, using Clojure and the Eclipse Paho client<a class="copiable-link" href="#Publishing-messages-with-MQTT_002c-using-Clojure-and-the-Eclipse-Paho-client"> &para;</a></span></h3>
<div id="_article_date_div">
<i class="i" id="_article_date">2013-06-02</i>
</div>
<a class="index-entry-id" id="index-2013-06"></a>
<a class="index-entry-id" id="index-Lisp-2"></a>
<a class="index-entry-id" id="index-Clojure"></a>
<a class="index-entry-id" id="index-MQTT-1"></a>

<p>I have found that <a class="uref" href="https://mqtt.org">MQTT</a> (MQ Telemetry Protocol)
is quite simple to use, and especially so when compared to most other
messaging protocols. In particular it&rsquo;s a great fit for embedded devices
or whenever resources are limited, since it&rsquo;s quite lightweight. From
the MQTT page:
</p>
<blockquote class="quotation">
<p>MQTT is a machine-to-machine (M2M)/”Internet of Things” connectivity
protocol. It was designed as an extremely lightweight publish/subscribe
messaging transport. It is useful for connections with remote locations
where a small code footprint is required and/or network bandwidth is at
a premium. For example, it has been used in sensors communicating to a
broker via satellite link, over occasional dial-up connections with
healthcare providers, and in a range of home automation and small device
scenarios. It is also ideal for mobile applications because of its small
size, low power usage, minimised data packets, and efficient
distribution of information to one or many receivers
</p></blockquote>
<p>On top of that I can see it being used when the needs themselves are
straightforward, which is often the case: just publish a payload to a
topic somewhere and avoid worrying with keeping up connection state,
error handling, etc. Additionally it has been proposed as an OASIS
standard which is good news for those looking for a messaging (and M2M
in particular)
</p>
<p>There are many implementation options to choose from (see the MQTT
software page for more details), this example code is based on the
Eclipse Paho implementation. See the
<a class="uref" href="https://gist.github.com/fsmunoz/5844443#file-mqtt-clj">full code on
the github gist</a>, the following is a snippet:
</p>
<div class="example user-clojure">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span><span style="color: #3D7B7B; font-style: italic">;; Simple example of MQTT message publish using Clojure</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Uses the Websphere Eclipse Paho client</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Author:   Frederico Munoz &lt;frederico.munoz@pt.ibm.com&gt;</span>
<span style="color: #3D7B7B; font-style: italic">;; Date:     18-Jun-2013</span>
<span style="color: #3D7B7B; font-style: italic">;; Keywords: mqtt, messaging, m2m, telemetry, clojure, iot, paho</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Copying and distribution of this file, with or without modification,</span>
<span style="color: #3D7B7B; font-style: italic">;; are permitted in any medium without royalty provided the copyright</span>
<span style="color: #3D7B7B; font-style: italic">;; notice and this notice are preserved.  This file is offered as-is,</span>
<span style="color: #3D7B7B; font-style: italic">;; without any warranty.</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; See http://www.eclipse.org/paho/ for the Eclipse Paho</span>
<span style="color: #3D7B7B; font-style: italic">;; implementation of MQTT, including the</span>
<span style="color: #3D7B7B; font-style: italic">;; org.eclipse.paho.client.mqttv3.jar JAR file which is used in this</span>
<span style="color: #3D7B7B; font-style: italic">;; example and should be in the classpath.</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; Use of leiningen is recommended for any more advanced use, but</span>
<span style="color: #3D7B7B; font-style: italic">;; assuming all the jars are in the current directory the following</span>
<span style="color: #3D7B7B; font-style: italic">;; should work:</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; $ java -cp clojure.jar:org.eclipse.paho.client.mqttv3.jar clojure.main mqtt.clj</span>
<span style="color: #3D7B7B; font-style: italic">;; Sat Jun 22 17:53:49 WEST 2013 : Sent</span>
<span style="color: #3D7B7B; font-style: italic">;; Connected to  tcp://m2m.eclipse.org:1883</span>
<span style="color: #3D7B7B; font-style: italic">;; *** PUBLISHING TO TOPIC  cljtest  ***</span>
<span style="color: #3D7B7B; font-style: italic">;; *** DELIVERY COMPLETE ***</span>
<span style="color: #3D7B7B; font-style: italic">;; $</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; This code only shows how to publish, but there are several MQTT</span>
<span style="color: #3D7B7B; font-style: italic">;; clients that can be used to subscribe to the topic. The Mosquitto</span>
<span style="color: #3D7B7B; font-style: italic">;; MQTT implementation (http://mosquitto.org/) comes with a client</span>
<span style="color: #3D7B7B; font-style: italic">;; which can be used thus:</span>
<span style="color: #3D7B7B; font-style: italic">;;</span>
<span style="color: #3D7B7B; font-style: italic">;; $ mosquitto_sub -h m2m.eclipse.org -t &quot;cljtest&quot; -v</span>
<span style="color: #3D7B7B; font-style: italic">;; cljtest But at least, out of my bitterness at what I&#39;ll never be, There&#39;s the quick calligraphy of these lines, The broken archway to the Impossible.</span>
<span style="color: #3D7B7B; font-style: italic">;; ^C</span>
<span style="color: #3D7B7B; font-style: italic">;; $</span>

(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttCallback</span>)
(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttClient</span>)
(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttConnectOptions</span>)
(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttDeliveryToken</span>)
(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttException</span>)
(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttMessage</span>)
(<span style="color: #008000">import </span><span style="color: #19177C">&#39;org.eclipse.paho.client.mqttv3.MqttTopic</span>)

<span style="color: #3D7B7B; font-style: italic">;; Main variables, most likely candidates for additional command line arguments</span>
<span style="color: #3D7B7B; font-style: italic">;; Change them according to your environment (the message can be supplied in the cli)</span>
(<span style="color: #008000; font-weight: bold">def </span><span style="color: #19177C">broker-url</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;tcp://m2m.eclipse.org:1883&quot;</span>)<span style="color: #bbbbbb"> </span><span style="color: #3D7B7B; font-style: italic">; Eclipse sandbox server, see http://m2m.eclipse.org/sandbox.html for details on use</span>
(<span style="color: #008000; font-weight: bold">def </span><span style="color: #19177C">mqtt-topic-name</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;cljtest&quot;</span>)
(<span style="color: #008000; font-weight: bold">def </span><span style="color: #19177C">mqtt-message</span><span style="color: #bbbbbb"> </span>(<span style="color: #008000; font-weight: bold">if </span>(<span style="color: #008000">seq </span>(<span style="color: #008000">first </span><span style="color: #19177C">*command-line-args*</span>))<span style="color: #bbbbbb">   </span><span style="color: #3D7B7B; font-style: italic">; Message, either the first</span>
<span style="color: #bbbbbb">                    </span>(<span style="color: #008000">first </span><span style="color: #19177C">*command-line-args*</span>)<span style="color: #bbbbbb">           </span><span style="color: #3D7B7B; font-style: italic">; command line argument or a snippet from Tobacco Shop</span>
<span style="color: #bbbbbb">                    </span><span style="color: #BA2121">&quot;But at least, out of my bitterness at what I&#39;ll never be, There&#39;s the quick calligraphy of these lines, The broken archway to the Impossible.&quot;</span>))
(<span style="color: #008000; font-weight: bold">def </span><span style="color: #19177C">client-id</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;FSM-Clj-Test&quot;</span>)

(<span style="color: #008000; font-weight: bold">defn- </span><span style="color: #19177C">mqtt-callback</span>
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Function called after delivery confirmation&quot;</span>
<span style="color: #bbbbbb">  </span>[]
<span style="color: #bbbbbb">  </span>(<span style="color: #0000FF">reify</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">MqttCallback</span>
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">connectionLost</span><span style="color: #bbbbbb"> </span>[<span style="color: #19177C">_</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">cause</span>]
<span style="color: #bbbbbb">      </span>(<span style="color: #008000">println </span>(<span style="color: #0000FF">.toString</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">cause</span>)))
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">messageArrived</span><span style="color: #bbbbbb"> </span>[<span style="color: #19177C">_</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">topic</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">message</span>]
<span style="color: #bbbbbb">      </span>(<span style="color: #008000">println </span><span style="color: #BA2121">&quot;Topic: &quot;</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">.getName</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">topic</span>))
<span style="color: #bbbbbb">      </span>(<span style="color: #008000">println </span><span style="color: #BA2121">&quot;Message: &quot;</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">.getPayload</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">message</span>)))
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">deliveryComplete</span><span style="color: #bbbbbb"> </span>[<span style="color: #19177C">_</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">token</span>]
<span style="color: #bbbbbb">      </span>(<span style="color: #008000">println </span><span style="color: #BA2121">&quot;*** DELIVERY COMPLETE ***&quot;</span>))))

(<span style="color: #008000; font-weight: bold">defn- </span><span style="color: #19177C">mqtt-connect</span>
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Establishes a MQTT connection to TOPIC; returns the mqtt client object&quot;</span>
<span style="color: #bbbbbb">  </span>[<span style="color: #19177C">topic</span>]
<span style="color: #bbbbbb">  </span>(<span style="color: #008000; font-weight: bold">let </span>[<span style="color: #19177C">mqtt-conn-options</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">MqttConnectOptions.</span>)
<span style="color: #bbbbbb">        </span><span style="color: #19177C">mqtt-client</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">MqttClient.</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">broker-url</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">client-id</span>)]
<span style="color: #bbbbbb">    </span>(<span style="color: #008000">doto </span><span style="color: #19177C">mqtt-conn-options</span>
<span style="color: #bbbbbb">      </span>(<span style="color: #0000FF">.setCleanSession</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">&#39;true</span>)
<span style="color: #bbbbbb">      </span>(<span style="color: #0000FF">.setKeepAliveInterval</span><span style="color: #bbbbbb"> </span><span style="color: #666666">30</span>))
<span style="color: #bbbbbb">    </span>(<span style="color: #008000">println </span><span style="color: #BA2121">&quot;Connected to &quot;</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">broker-url</span>)
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">.setCallback</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">mqtt-client</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">mqtt-callback</span>))
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">.connect</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">mqtt-client</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">mqtt-conn-options</span>)
<span style="color: #bbbbbb">    </span><span style="color: #19177C">mqtt-client</span>))

(<span style="color: #008000; font-weight: bold">defn- </span><span style="color: #19177C">mqtt-create-message</span>
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Creates a MQTT message from MESSAGE, a string&quot;</span>
<span style="color: #bbbbbb">  </span>[<span style="color: #19177C">message</span>]
<span style="color: #bbbbbb">  </span>(<span style="color: #008000; font-weight: bold">let </span>[<span style="color: #19177C">mqtt-message</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">MqttMessage.</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">.getBytes</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">message</span>))]
<span style="color: #bbbbbb">    </span>(<span style="color: #008000">doto </span><span style="color: #19177C">mqtt-message</span>
<span style="color: #bbbbbb">      </span>(<span style="color: #0000FF">.setQos</span><span style="color: #bbbbbb">  </span><span style="color: #666666">0</span>)
<span style="color: #bbbbbb">      </span>(<span style="color: #0000FF">.setRetained</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">false</span>))))

(<span style="color: #008000; font-weight: bold">defn- </span><span style="color: #19177C">mqtt-publish</span>
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Publishes MESSAGE to TOPIC&quot;</span>
<span style="color: #bbbbbb">  </span>[<span style="color: #19177C">topic</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">message</span>]
<span style="color: #bbbbbb">  </span>(<span style="color: #008000">println </span><span style="color: #BA2121">&quot;*** PUBLISHING TO TOPIC &quot;</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">.toString</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">topic</span>)<span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot; ***&quot;</span>)
<span style="color: #bbbbbb">  </span>(<span style="color: #0000FF">.waitForCompletion</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">.publish</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">topic</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">message</span>)))

(<span style="color: #008000; font-weight: bold">defn </span><span style="color: #19177C">testmq</span>
<span style="color: #bbbbbb">  </span><span style="color: #BA2121">&quot;Main demo function, creates the connection and sends the message&quot;</span>
<span style="color: #bbbbbb">  </span>[]
<span style="color: #bbbbbb">  </span>(<span style="color: #008000; font-weight: bold">let </span>[<span style="color: #19177C">client</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">mqtt-connect</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">mqtt-topic-name</span>)
<span style="color: #bbbbbb">        </span><span style="color: #19177C">topic</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">.getTopic</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">client</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">mqtt-topic-name</span>)
<span style="color: #bbbbbb">        </span><span style="color: #19177C">message</span><span style="color: #bbbbbb"> </span>(<span style="color: #0000FF">mqtt-create-message</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">mqtt-message</span>)]
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">mqtt-publish</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">topic</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">message</span>)
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">Thread/sleep</span><span style="color: #bbbbbb"> </span><span style="color: #666666">100</span>)
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">.disconnect</span><span style="color: #bbbbbb"> </span><span style="color: #19177C">client</span>)
<span style="color: #bbbbbb">    </span>(<span style="color: #0000FF">shutdown-agents</span>)))

<span style="color: #3D7B7B; font-style: italic">;; Make it all work</span>
(<span style="color: #0000FF">testmq</span>)

<span style="color: #3D7B7B; font-style: italic">;; End of file</span>
</pre></div>
</pre></div>

<p>Usage is trivial, here&rsquo;s an example:
</p>
<div class="example user-shell">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span>$<span style="color: #bbbbbb"> </span>java<span style="color: #bbbbbb"> </span>-cp<span style="color: #bbbbbb"> </span>~/.m2/repository/org/clojure/clojure/1.5.1/clojure-1.5.1.jar:/tmp/dummy/org.eclipse.paho.client.mqttv3.jar<span style="color: #bbbbbb"> </span>clojure.main<span style="color: #bbbbbb"> </span>mqtt.clj
Connected<span style="color: #bbbbbb"> </span>to<span style="color: #bbbbbb"> </span>tcp://m2m.eclipse.org:1883
***<span style="color: #bbbbbb"> </span>PUBLISHING<span style="color: #bbbbbb"> </span>TO<span style="color: #bbbbbb"> </span>TOPIC<span style="color: #bbbbbb"> </span>cljtest<span style="color: #bbbbbb"> </span>***
***<span style="color: #bbbbbb"> </span>DELIVERY<span style="color: #bbbbbb"> </span>COMPLETE<span style="color: #bbbbbb"> </span>***
$
</pre></div>
</pre></div>

<p>... and seeing the publishing using the Mosquitto MQTT client to
subscribe to the topic:
</p>
<div class="example user-shell">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span>$<span style="color: #bbbbbb"> </span>mosquitto_sub<span style="color: #bbbbbb"> </span>-h<span style="color: #bbbbbb"> </span>m2m.eclipse.org<span style="color: #bbbbbb"> </span>-t<span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;cljtest&quot;</span><span style="color: #bbbbbb"> </span>-v
cljtest<span style="color: #bbbbbb"> </span>But<span style="color: #bbbbbb"> </span>at<span style="color: #bbbbbb"> </span>least,<span style="color: #bbbbbb"> </span>out<span style="color: #bbbbbb"> </span>of<span style="color: #bbbbbb"> </span>my<span style="color: #bbbbbb"> </span>bitterness<span style="color: #bbbbbb"> </span>at<span style="color: #bbbbbb"> </span>what<span style="color: #bbbbbb"> </span>I<span style="color: #BA2121">&#39;ll never be, There&#39;</span>s<span style="color: #bbbbbb"> </span>the<span style="color: #bbbbbb"> </span>quick<span style="color: #bbbbbb"> </span>calligraphy<span style="color: #bbbbbb"> </span>of<span style="color: #bbbbbb"> </span>these<span style="color: #bbbbbb"> </span>lines,<span style="color: #bbbbbb"> </span>The<span style="color: #bbbbbb"> </span>broken<span style="color: #bbbbbb"> </span>archway<span style="color: #bbbbbb"> </span>to<span style="color: #bbbbbb"> </span>the<span style="color: #bbbbbb"> </span>Impossible.
^C
$
</pre></div>
</pre></div>

<p>Yet again Clojure presents itself as a good way to take advantage of the
Java libs, even when one doesn&rsquo;t use most of other Clojure&rsquo;s strengths.
</p></div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
