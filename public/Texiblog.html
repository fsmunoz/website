<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Texiblog (interlaye.red)</title>

<meta name="description" content="Texiblog (interlaye.red)">
<meta name="keywords" content="Texiblog (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Projects.html" rel="up" title="Projects">
<link href="Voting-Analysis.html" rel="next" title="Voting Analysis">
<link href="Budotree.html" rel="prev" title="Budotree">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.center {text-align:center}
div.example {margin-left: 3.2em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Texiblog.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Texiblog">
<div class="nav-panel">
<p>
Next: <a href="Voting-Analysis.html" accesskey="n" rel="next">Voting Analysis</a>, Previous: <a href="Budotree.html" accesskey="p" rel="prev">Budō Lineage Tree</a>, Up: <a href="Projects.html" accesskey="u" rel="up">Projects</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Texiblog-1"><span>Texiblog<a class="copiable-link" href="#Texiblog-1"> &para;</a></span></h3>

<blockquote class="quotation">
<p>Texinfo is the official documentation format of the GNU project. It is
used by many non-GNU projects as well.
</p></blockquote>
<div class="center">&mdash; <em class="emph">GNU Texinfo manual</em>
</div>
<p><code class="command">texiblog</code> is my home-grown solution for building a static
site, and is what is behind my personal website. It uses
<code class="command">texi2any</code> to create the HTML from Texinfo files, adds some
styling options, creates updated menus from the posts (to provide an
easy way to check recent articles), and uses <code class="command">m4</code> throughout
as a pre-processor and as a way to create the Atom and RSS XML files,
as well as other niceties.
</p>
<p>The code is available at the <a class="url" href="https://notabug.org/fsm/texiblog"><code class="code">texiblog</code> Notabug repository</a>.
</p>
<p>In the next sections I will go a bit more in-depth about it, but the
short version is that <em class="emph">using Texinfo produces a content-focused
website, with minimal distractions, easily viewable through any
browser, and makes it natural to maintain a structure that is
predictable, easy to use, and maps well into other formats, such as a
PDF, or a standalone <samp class="file">.info</samp> file</em>.
</p>
<p>There&rsquo;s some traction on &ldquo;Brutalism in web design&rdquo;, and if taken in
one of its forms &ndash; the one that focus on showing content with minimal
distractions, as described in <a class="url" href="https://brutalist-web.design/">David Copeland&rsquo;s &ldquo;Guidelines for Brutalist Web Design&rdquo;</a>, a good
description of this approach &ndash; this approach closely follows it, but
not because of an explicit desire to do so, but because those are the
results of using the Texinfo toolchain (See <a class="xref" href="Texinfo-for-the-web.html">Creating a web page and blog, the Texinfo way.</a>).
</p>
<div class="subsection-level-extent" id="Why-Texinfo-for-a-personal-site_002e">
<h4 class="subsection"><span>Why Texinfo for a personal site.<a class="copiable-link" href="#Why-Texinfo-for-a-personal-site_002e"> &para;</a></span></h4>

<p>There are a lot of &ldquo;markdown&rdquo; flavours out there, and I have
previously (and currently) used AsciiDoc and Markdown.
</p>
<p>I am far from the first one using this approach: using TeX for
websites was reasonably common, and one will find examples
<a class="url" href="https://www.jwz.org/doc/worse-is-better.html">as this &ldquo;Worse is
Better&rdquo; copy on Jamie Zawinki&rsquo;s site</a>, which were initially created
with <code class="command">latex2html</code>.
</p>
<p>Texinfo is simpler than LaTeX, and its usage as the documentation
format for the GNU project makes it come with some really good
features for a static web site, especially when the goal is to
<em class="emph">keep focus on the content</em>.
</p>
<h4 class="subsubheading" id="Stargrave_0027s-example"><span>Stargrave&rsquo;s example<a class="copiable-link" href="#Stargrave_0027s-example"> &para;</a></span></h4>

<p>When I started to think about this idea I went searching for existing
examples, and I found <a class="url" href="http://www.stargrave.org/InfoRules.html">Sergey Matveev&rsquo;s homepage</a>, which is not only created using Texinfo
(<code class="command">texi2html</code>, most likely), he does an in-depth comparison of
formats and why he chose Texinfo. The entire site had exactly the
&ldquo;feel&rdquo; I was after: good indexing, simple navigation that is easy to
use, and a degree of familiarity that comes from reading a lot of GNU
documentation (in <code class="command">info</code> or converted to HTML), which I find
&ldquo;clean&rdquo;.
</p>
<h4 class="subsubheading" id="Technomancy-and-m4"><span>Technomancy and m4<a class="copiable-link" href="#Technomancy-and-m4"> &para;</a></span></h4>

<p>Texinfo already comes with <code class="command">texi2any</code>, the current replacement
of <code class="command">texi2HTML</code>, so much of the heavy-lifting is already done
for us. I needed some extra features, and for that
<a class="url" href="https://technomancy.us/colophon">Phil Hagelberg &ndash; aka
<code class="code">technomancy</code></a> provided additional inspiration: his entire
website is made through a Makefile and <code class="command">m4</code> &ndash; which some of
you will remember as the language used for the configuration of
Sendmail, at least those that didn&rsquo;t directly edited the generated
file &ndash; which it&rsquo;s awesome in itself, but also made me investigate how
could I leverage parts of it for my solution.
</p>
<p><a class="url" href="https://chrisman.github.io/9.html">Chrisman Brown does a good
description of the <code class="command">m4</code> approach</a> used there, and it made be
learn a bit more about <code class="command">m4</code>: I initially had a bunch of
scripts that worked on templates, and in the end I was left with only
the Makefile, with <code class="command">m4</code> doing the work.
</p>

</div>
<div class="subsection-level-extent" id="What-does-it-do">
<h4 class="subsection"><span>What does it do<a class="copiable-link" href="#What-does-it-do"> &para;</a></span></h4>

<p>Obviously, it creates web output from Texinfo files, using
<code class="code">texi2any</code>, but there&rsquo;s more going on.
</p>
<dl class="table">
<dt><em class="dfn"><strong class="strong">Makefile</strong></em></dt>
<dd>
<p>The Makefile defines all targets and uses <code class="command">m4</code>, <code class="command">awk</code>,
and a couple of other standard tools to create the site.
</p>
</dd>
<dt><em class="dfn"><strong class="strong">M4 configuration files</strong></em></dt>
<dd>
<p>The main configuration is done with <code class="command">m4</code>, which is used to
generate any Texinfo file that needs processing, as well as defining
the main configuration variables (e.g., website title).
</p>
<div class="example">
<pre class="example-preformatted">$ ls -1 src/*.m4
src/atom.m4
src/config.m4
src/config.texi.m4
src/feed.m4
src/posts.m4
src/rss.m4
</pre></div>

</dd>
<dt><em class="dfn"><strong class="strong">CSS</strong></em></dt>
<dd>
<p>There&rsquo;s a case for not adding anything to the output: the result is
indeed ready to use. I have opted for some tweaks, especially in terms
of maximum width to keep the text more readable, as well as some
slight changes to how the page works on different screen sizes.
</p>
</dd>
<dt><em class="dfn"><strong class="strong">Atom and RSS feeds</strong></em></dt>
<dd>
<p>It&rsquo;s also <code class="command">m4</code> that creates Atom XML and RSS XML files, which
are then included in the generated files.
</p>
</dd>
<dt><em class="dfn"><strong class="strong">Post descriptions and dates</strong></em></dt>
<dd>
<p><code class="command">texi2any</code> has a nifty feature that does without the need to
manually create
<a class="url" href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/Menus.html">Texinfo menus by hand</a>: when exporting to HTML, it creates a table of
contents for any sections &ldquo;under&rdquo; the Top node.
</p>
<p>This would work great, except for two things: using a menu allows me
to add a description to the entry, which in the case of posts
(i.e. the &ldquo;blog&rdquo; aspect of this) can be used to indicate the
date. Additionally, it&rsquo;s useful to have control on what gets shown in
some sections, especially at the very start of the page where I want
to give quick links to the most important sections.
</p>
<p>The downside of this is that automatically generated menus do not have
a description, so <code class="command">m4</code> (again) goes through all files under
<samp class="file">src/posts/</samp> and creates a menu using the <code class="code">@date</code> field.
</p>
</dd>
<dt><em class="dfn"><strong class="strong">Texinfo macros</strong></em></dt>
<dd>
<p>Some macros are added to either simplify some things, or make other
possible: the <code class="code">@date</code> macro is important to make the previously
described menu automation work, while others such as <code class="code">@imagec</code>
simplify adding a float, with an image and a caption.
</p>
</dd>
<dt><em class="dfn"><strong class="strong">Post-processing</strong></em></dt>
<dd>
<p>This is a bit open-ended, but currently includes changing the
navigation bar to always include a link to the home page (that&rsquo;s the
<code class="code">[Top]</code> link that appears last, and which isn&rsquo;t a part of the
default navbar).
</p></dd>
</dl>

<p>More than 90% of the approach is simply based on writing Texinfo files.
</p>
</div>
</div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
