<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>On the Ada language and embedded development (interlaye.red)</title>

<meta name="description" content="On the Ada language and embedded development (interlaye.red)">
<meta name="keywords" content="On the Ada language and embedded development (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Posts.html" rel="up" title="Posts">
<link href="Common-Lisp-and-MQTT-with-ABCL.html" rel="next" title="Common Lisp and MQTT with ABCL">
<link href="Embedded-Development-with-uLisp.html" rel="prev" title="Embedded Development with uLisp">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Arduino-Programming-with-Ada.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Arduino-Programming-with-Ada">
<div class="nav-panel">
<p>
Next: <a href="Common-Lisp-and-MQTT-with-ABCL.html" accesskey="n" rel="next">Using MQTT in Common Lisp with ABCL and the Eclipse Paho client.</a>, Previous: <a href="Embedded-Development-with-uLisp.html" accesskey="p" rel="prev">Interactive embedded development with uLisp, Arduino and Emacs</a>, Up: <a href="Posts.html" accesskey="u" rel="up">Posts</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="On-the-Ada-language-and-embedded-development"><span>On the Ada language and embedded development<a class="copiable-link" href="#On-the-Ada-language-and-embedded-development"> &para;</a></span></h3>
<div id="_article_date_div">
<i class="i" id="_article_date">2017-08-02</i>
</div>
<a class="index-entry-id" id="index-2017-08-1"></a>
<a class="index-entry-id" id="index-Ada"></a>
<a class="index-entry-id" id="index-Arduino-1"></a>

<p>I&rsquo;ve been recently drawn to <a class="uref" href="https://www.adacore.com/about-ada">Ada</a>
(once again). Now, I&rsquo;m a Lisper through and through and in a way Ada is
the exact opposite of Lisp, especially in terms of typing, but perhaps
because of that I find it quite interesting. Showing the end result
right now, here is the canonical blinking LED done in Ada:
</p>
<div class="float">
<img class="image" src="images/ada-blink.jpg" alt="images/ada-blink">
<div class="caption"><p>Arduino blinking through Ada code</p></div></div>
<p>Searching around I found the <a class="uref" href="http://www.makewithada.org/">Make With
Ada competition</a>, and I went looking for ways to use Ada with what I had
immediately available. I found the
<a class="uref" href="https://sourceforge.net/p/avr-ada/wiki/Home/">AVR-Ada project</a>
which looked great. The build process depends on specific versions of
the different components (which is quite common), and I spent some time
following the manual build process&hellip; but then I remembered that I
use Docker as a way to encapsulate certain environments.
</p>
<p>Since I didn&rsquo;t found an existing AVR-Ada image I created one, built on
the shoulders of
<a class="uref" href="http://arduino.ada-language.com/author/tero-koskinen.html">Tero
Koskinen‘s Fedora 25 RPMs</a>,and uploaded them to the
<a class="uref" href="https://hub.docker.com/r/fsmunoz/avr-ada/">Docker hub</a>. The goal is
to reduce the “cost of entry” for using AVR-Ada.
</p>
<div class="subsection-level-extent" id="Building-it">
<h4 class="subsection"><span>Building it<a class="copiable-link" href="#Building-it"> &para;</a></span></h4>
<a class="anchor" id="g_t_0023building_002dit"></a><p>As an example, building a typical “make the internal LED of the Arduino
blink” solution will be as simple as:
</p>
<ul class="itemize mark-bullet">
<li>Downloading the docker image
</li></ul>

<div class="example user-shell">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span>docker<span style="color: #bbbbbb"> </span>pull<span style="color: #bbbbbb"> </span>fsmunoz/avr-ada
</pre></div>
</pre></div>

<ul class="itemize mark-bullet">
<li>Create the container with something like the following (I use alias for
most docker containers and then just use them as commands in the
terminal):
</li></ul>

<div class="example user-shell">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span><span style="color: #008000">alias</span><span style="color: #bbbbbb"> </span>avr-ada<span style="color: #666666">=</span><span style="color: #BA2121">&quot;docker run -it --rm \</span>
<span style="color: #BA2121"> -v </span><span style="color: #19177C">$HOME</span><span style="color: #BA2121">/src:/src:Z \</span>
<span style="color: #BA2121"> --device /dev/ttyACM0 \</span>
<span style="color: #BA2121"> --name avr-ada \</span>
<span style="color: #BA2121"> --group-add dialout \</span>
<span style="color: #BA2121"> fsmunoz/avr-ada&quot;</span>
</pre></div>
</pre></div>

<ul class="itemize mark-bullet">
<li>Create an initial project; I would suggest Tero&rsquo;s blinking led
repository. To get an idea of what the code looks like, here it is
(also,
<a class="uref" href="https://gist.github.com/fsmunoz/5865663537966937540a8090b675bcc9#file-blink-adb">here&rsquo;s
the gist in GitHub</a>:
</li></ul>

<div class="example user-ada">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span><span style="color: #3D7B7B; font-style: italic">-- Blink the internal LED of an Arduino (and others) using AVR-Ada</span>
<span style="color: #3D7B7B; font-style: italic">--</span>
<span style="color: #3D7B7B; font-style: italic">-- Code by Tero Koskinen</span>
<span style="color: #3D7B7B; font-style: italic">-- http://arduino.ada-language.com/blinking-led.html</span>

<span style="color: #008000; font-weight: bold">with</span> AVR;
<span style="color: #008000; font-weight: bold">with</span> AVR.MCU;
<span style="color: #008000; font-weight: bold">with</span> AVR.Real_Time.Delays;

<span style="color: #008000; font-weight: bold">use</span> AVR;

<span style="color: #008000; font-weight: bold">procedure</span> <span style="color: #0000FF">Blink</span> <span style="color: #008000; font-weight: bold">is</span>
   Led_On : <span style="color: #B00040">Boolean</span> := <span style="color: #008000; font-weight: bold">False</span>;
   <span style="color: #880000">LED_PIN</span> : <span style="color: #008000; font-weight: bold">constant</span> := <span style="color: #666666">5</span>;

<span style="color: #008000; font-weight: bold">begin</span>
   MCU.DDRB_Bits (LED_PIN) := DD_Output;
   <span style="color: #008000; font-weight: bold">loop</span>
      Led_On := <span style="color: #AA22FF; font-weight: bold">not</span> Led_On;
      MCU.PORTB_Bits (LED_PIN) := Led_On;
      <span style="color: #008000; font-weight: bold">delay</span> <span style="color: #666666">8.0</span>;
   <span style="color: #008000; font-weight: bold">end</span> <span style="color: #008000; font-weight: bold">loop</span>;
<span style="color: #008000; font-weight: bold">end</span> <span style="color: #0000FF">Blink</span>;
</pre></div>
</pre></div>

<ul class="itemize mark-bullet">
<li>In the docker image navigate to the source dir (which is why in the
example above you can see I mapped ~/src to /src in the container).

</li><li>Do a “make” and a “make upload”; something like this:

</li></ul>

<div class="example user-shell">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span>$<span style="color: #bbbbbb"> </span>make<span style="color: #bbbbbb"> </span>blink.prog
avr-gnatmake<span style="color: #bbbbbb"> </span>-XMCU<span style="color: #666666">=</span>atmega328p<span style="color: #bbbbbb"> </span>-p<span style="color: #bbbbbb"> </span>-Pbuild.gpr<span style="color: #bbbbbb"> </span>-XAVRADA_MAIN<span style="color: #666666">=</span>blink
avr-gnatmake:<span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;/src/ada/arduino/blink/blink.elf&quot;</span><span style="color: #bbbbbb"> </span>up<span style="color: #bbbbbb"> </span>to<span style="color: #bbbbbb"> </span>date.
avrdude<span style="color: #bbbbbb"> </span>-p<span style="color: #bbbbbb"> </span>atmega328p<span style="color: #bbbbbb"> </span>-P<span style="color: #bbbbbb"> </span>/dev/ttyACM0<span style="color: #bbbbbb"> </span>-c<span style="color: #bbbbbb"> </span>arduino<span style="color: #bbbbbb"> </span>-U<span style="color: #bbbbbb"> </span>flash:w:blink.hex

avrdude:<span style="color: #bbbbbb"> </span>AVR<span style="color: #bbbbbb"> </span>device<span style="color: #bbbbbb"> </span>initialized<span style="color: #bbbbbb"> </span>and<span style="color: #bbbbbb"> </span>ready<span style="color: #bbbbbb"> </span>to<span style="color: #bbbbbb"> </span>accept<span style="color: #bbbbbb"> </span>instructions

Reading<span style="color: #bbbbbb"> </span>|<span style="color: #bbbbbb"> </span><span style="color: #3D7B7B; font-style: italic">################################################## | 100% 0.00s</span>

avrdude:<span style="color: #bbbbbb"> </span>Device<span style="color: #bbbbbb"> </span><span style="color: #19177C">signature</span><span style="color: #bbbbbb"> </span><span style="color: #666666">=</span><span style="color: #bbbbbb"> </span>0x1e950f<span style="color: #bbbbbb"> </span><span style="color: #666666">(</span>probably<span style="color: #bbbbbb"> </span>m328p<span style="color: #666666">)</span>
avrdude:<span style="color: #bbbbbb"> </span>NOTE:<span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;flash&quot;</span><span style="color: #bbbbbb"> </span>memory<span style="color: #bbbbbb"> </span>has<span style="color: #bbbbbb"> </span>been<span style="color: #bbbbbb"> </span>specified,<span style="color: #bbbbbb"> </span>an<span style="color: #bbbbbb"> </span>erase<span style="color: #bbbbbb"> </span>cycle<span style="color: #bbbbbb"> </span>will<span style="color: #bbbbbb"> </span>be<span style="color: #bbbbbb"> </span>performed
<span style="color: #bbbbbb"> </span>To<span style="color: #bbbbbb"> </span>disable<span style="color: #bbbbbb"> </span>this<span style="color: #bbbbbb"> </span>feature,<span style="color: #bbbbbb"> </span>specify<span style="color: #bbbbbb"> </span>the<span style="color: #bbbbbb"> </span>-D<span style="color: #bbbbbb"> </span>option.
avrdude:<span style="color: #bbbbbb"> </span>erasing<span style="color: #bbbbbb"> </span>chip
avrdude:<span style="color: #bbbbbb"> </span>reading<span style="color: #bbbbbb"> </span>input<span style="color: #bbbbbb"> </span>file<span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;blink.hex&quot;</span>
avrdude:<span style="color: #bbbbbb"> </span>input<span style="color: #bbbbbb"> </span>file<span style="color: #bbbbbb"> </span>blink.hex<span style="color: #bbbbbb"> </span>auto<span style="color: #bbbbbb"> </span>detected<span style="color: #bbbbbb"> </span>as<span style="color: #bbbbbb"> </span>Intel<span style="color: #bbbbbb"> </span>Hex
avrdude:<span style="color: #bbbbbb"> </span>writing<span style="color: #bbbbbb"> </span>flash<span style="color: #bbbbbb"> </span><span style="color: #666666">(1096</span><span style="color: #bbbbbb"> </span>bytes<span style="color: #666666">)</span>:

Writing<span style="color: #bbbbbb"> </span>|<span style="color: #bbbbbb"> </span><span style="color: #3D7B7B; font-style: italic">################################################## | 100% 0.19s</span>

avrdude:<span style="color: #bbbbbb"> </span><span style="color: #666666">1096</span><span style="color: #bbbbbb"> </span>bytes<span style="color: #bbbbbb"> </span>of<span style="color: #bbbbbb"> </span>flash<span style="color: #bbbbbb"> </span>written
avrdude:<span style="color: #bbbbbb"> </span>verifying<span style="color: #bbbbbb"> </span>flash<span style="color: #bbbbbb"> </span>memory<span style="color: #bbbbbb"> </span>against<span style="color: #bbbbbb"> </span>blink.hex:
avrdude:<span style="color: #bbbbbb"> </span>load<span style="color: #bbbbbb"> </span>data<span style="color: #bbbbbb"> </span>flash<span style="color: #bbbbbb"> </span>data<span style="color: #bbbbbb"> </span>from<span style="color: #bbbbbb"> </span>input<span style="color: #bbbbbb"> </span>file<span style="color: #bbbbbb"> </span>blink.hex:
avrdude:<span style="color: #bbbbbb"> </span>input<span style="color: #bbbbbb"> </span>file<span style="color: #bbbbbb"> </span>blink.hex<span style="color: #bbbbbb"> </span>auto<span style="color: #bbbbbb"> </span>detected<span style="color: #bbbbbb"> </span>as<span style="color: #bbbbbb"> </span>Intel<span style="color: #bbbbbb"> </span>Hex
avrdude:<span style="color: #bbbbbb"> </span>input<span style="color: #bbbbbb"> </span>file<span style="color: #bbbbbb"> </span>blink.hex<span style="color: #bbbbbb"> </span>contains<span style="color: #bbbbbb"> </span><span style="color: #666666">1096</span><span style="color: #bbbbbb"> </span>bytes
avrdude:<span style="color: #bbbbbb"> </span>reading<span style="color: #bbbbbb"> </span>on-chip<span style="color: #bbbbbb"> </span>flash<span style="color: #bbbbbb"> </span>data:

Reading<span style="color: #bbbbbb"> </span>|<span style="color: #bbbbbb"> </span><span style="color: #3D7B7B; font-style: italic">################################################## | 100% 0.15s</span>

avrdude:<span style="color: #bbbbbb"> </span>verifying<span style="color: #bbbbbb"> </span>...
avrdude:<span style="color: #bbbbbb"> </span><span style="color: #666666">1096</span><span style="color: #bbbbbb"> </span>bytes<span style="color: #bbbbbb"> </span>of<span style="color: #bbbbbb"> </span>flash<span style="color: #bbbbbb"> </span>verified

avrdude:<span style="color: #bbbbbb"> </span>safemode:<span style="color: #bbbbbb"> </span>Fuses<span style="color: #bbbbbb"> </span>OK<span style="color: #bbbbbb"> </span><span style="color: #666666">(</span>E:00,<span style="color: #bbbbbb"> </span>H:00,<span style="color: #bbbbbb"> </span>L:00<span style="color: #666666">)</span>

avrdude<span style="color: #bbbbbb"> </span><span style="color: #008000; font-weight: bold">done</span>.<span style="color: #bbbbbb"> </span>Thank<span style="color: #bbbbbb"> </span>you.
</pre></div>
</pre></div>

<ul class="itemize mark-bullet">
<li><i class="i">Relaxen und watchen das blinkenlichten.</i>
</li></ul>

<p>On a more general note I confess that I find the typical objections to
Ada tiresome, especially the “bondage and discipline language designed
by committee”. I also read the Jargon File and I was raised in that very
same culture, but it comes a time when one needs to go a bit deeper, and
the “designed by committee” objection is more of an ideological
statement than a practical one. Additionally, I have read some comments
on how AdaCore (the main company behind Ada technoloy, but no the only
one) is somehow &quot;wrong&quot; in supplying all the tools
(<a class="uref" href="https://www.adacore.com/download">GNAT</a>) but not providing an exception clause to the GPL. I must say that
I find this usage perfectly natural and to the spirit of the GPL: the
exception clause on GCC was always a tactical measure, not a strategic
one, and AdaCore is as far as I know working steadily on the GPL
edition, and this is actually the whole idea behind the GPL.
</p>
<p>This is a completely different topic but the “business friendly” mantra
that BSDers in general like to use is mostly the liberty for companies
to use code and then close it up and profit, and this is seen as good &ndash;
<em class="emph">but when a company opens up the code by using the GPL and thus
benefits users</em> it is seen as &quot;unfriendly&quot;. As with most things an
analysis of good and bad is dependent on who has to gain &mdash; cui bono?
This is perhaps worthy of a different post.
</p></div>
</div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
