<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Creating a web page and blog, the Texinfo way. (interlaye.red)</title>

<meta name="description" content="Creating a web page and blog, the Texinfo way. (interlaye.red)">
<meta name="keywords" content="Creating a web page and blog, the Texinfo way. (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Posts.html" rel="up" title="Posts">
<link href="Voting-Analysis-Update.html" rel="next" title="Voting Analysis Update">
<link href="Kubernetes-1_002e29-Release-Notes-Lead.html" rel="prev" title="Kubernetes 1.29 Release Notes Lead">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.center {text-align:center}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?Texinfo-for-the-web.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="Texinfo-for-the-web">
<div class="nav-panel">
<p>
Next: <a href="Voting-Analysis-Update.html" accesskey="n" rel="next">Analysing political distance by voting behaviour alone.</a>, Previous: <a href="Kubernetes-1_002e29-Release-Notes-Lead.html" accesskey="p" rel="prev">Excipit Kubernetes v1.28, Incipit Kubernetes v1.29</a>, Up: <a href="Posts.html" accesskey="u" rel="up">Posts</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Creating-a-web-page-and-blog_002c-the-Texinfo-way_002e"><span>Creating a web page and blog, the Texinfo way.<a class="copiable-link" href="#Creating-a-web-page-and-blog_002c-the-Texinfo-way_002e"> &para;</a></span></h3>
<div id="_article_date_div">
<i class="i" id="_article_date">2023-08-10</i>
</div>
<a class="index-entry-id" id="index-Texinfo"></a>
<a class="index-entry-id" id="index-2023-08-1"></a>

<blockquote class="quotation">
<p>A website&rsquo;s materials aren&rsquo;t HTML tags, CSS, or JavaScript
code. Rather, they are its content and the context in which it&rsquo;s
consumed. A website is for a visitor, using a browser, running on a
computer to read, watch, listen, or perhaps to interact. A website
that embraces Brutalist Web Design is raw in its focus on content, and
prioritization of the website visitor. 
</p>

</blockquote>
<div class="center">&mdash; <em class="emph"><a class="url" href="https://brutalist-web.design/">Guidelines for Brutalist Web Design</a></em>
</div>
<p>We all know that personal web pages and blogs have lost
popularity. I&rsquo;ve now solved the main reason behind it: no easy way to
do it using <a class="url" href="https://www.gnu.org/software/texinfo/">Texinfo</a>.
</p>
<div class="subsection-level-extent" id="What-is-it">
<h4 class="subsection"><span>What is it<a class="copiable-link" href="#What-is-it"> &para;</a></span></h4>

<p><code class="code">texiblog</code> is the result of my journey trying to find a simple
solution for restarting a personal website, with updated articles &ndash;
so, a website that is also a &ldquo;blog&rdquo;. There is no lack of solutions
out there, from Wordpress to static site generators, but I didn&rsquo;t
found one that completely satisfied me.
</p>
<div class="float">
<img class="image" src="images/texiblog_lynx.png" alt="images/texiblog_lynx">
<div class="caption"><p>The result viewed in <code class="command">lynx</code>: completely usable.</p></div></div>
<p><a class="ref" href="Texiblog.html">I describe it more thoroughly in the Project section</a>:
</p><blockquote class="quotation">

<p><code class="command">texiblog</code> is my home-grown solution for building a static
site, and is what is behind my personal website. It uses
<code class="command">texi2any</code> to create the HTML from Texinfo files, adds some
styling options, creates updated menus from the posts (to provide an
easy way to check recent articles), and uses <code class="command">m4</code> throughout
as a pre-processor and as a way to create the Atom and RSS XML files,
as well as other niceties.
</p></blockquote>

<p>So, there you have it: a Texinfo website, with some minimal styling
meant to keep the inherent simplicity of the approach, that uses
<code class="command">m4</code> for most of the things that aren&rsquo;t directly done by
<code class="command">texi2any</code> (included in Texinfo). It outputs HTML, ready to
deploy anywhere, but can also create a <samp class="file">info</samp> file, or a PDF, or
everything in a single text file.
</p>
<p>The code is available at the <a class="url" href="https://notabug.org/fsm/texiblog"><code class="code">texiblog</code> Notabug repository</a>.
</p>

</div>
<div class="subsection-level-extent" id="What_0027s-Brutalist-about-it_003f">
<h4 class="subsection"><span>What&rsquo;s Brutalist about it?<a class="copiable-link" href="#What_0027s-Brutalist-about-it_003f"> &para;</a></span></h4>
<div class="float">
<img class="image" src="images/sb1.jpg" alt="images/sb1">
<div class="caption"><p>Het Poplakov Cafe (Dnipropetrovsk),
Ukraine. A popular example of Soviet brutalism with a space-travel
twist.</p></div></div>
<p>Very little and, amazingly, also a lot, depending on how you look at
it.
</p>
<p>The topic of how Brutalism applies to web design has seen some
interesting discussions in recent years, also tied with the increased
appreciation for the architectural style. There are two main
approaches, which are also largely incompatible:
</p>
<dl class="table">
<dt><em class="dfn">Brutalism as a retro-aesthetic</em></dt>
<dd>
<p>This is the most visible one, and also the one that is noticeable more
purposeful: websites apply complex designs that make them look
&ldquo;raw&rdquo;, often using a colour palette and fonts reminiscent of the
ones used in the 70s-80s. The focus is more a recreation of the
aesthetics around Brutalist architecture, including things that were
not part of it but that were used at the same time.
</p>
<p>The gallery at <a class="url" href="https://brutalistwebsites.com/">Brutalist
Websites</a> shows many such sites.
</p>
</dd>
<dt><em class="dfn">Brutalism as adherence to the original principles</em></dt>
<dd>
<p>This is less concerned with recreating a particular aesthetics, even
though it ends up having one due to the application of the identified
principles &ndash; one that often recreates early web pages, with minimal
or no CSS at all and raw HTML elements used in their default
forms. This applies Brutalism to web design by stressing
<em class="emph">content</em> as the raw material, and everything else should revolve
around it.
</p>
<p><a class="url" href="https://brutalist-web.design/">David Copeland&rsquo;s &ldquo;Guidelines for
Brutalist Web Design&rdquo;</a> is a great example of this.
</p>
</dd>
</dl>

<p>By using Texinfo, and the output produced by <code class="command">texi2any</code>,
there&rsquo;s by default a &ldquo;rawness&rdquo; of the latter kind, given the little
to no styling done by default. The additional styling I did (maximum
width, side images in large screens) was made in a way that keeps the
focus on the principles identified there. This was not done initially
on purpose: I found &ldquo;Brutalist web design&rdquo; while searching around
for minimal styling suggestions that kept content at the centre stage,
and the main concepts behind it are already well-served by the default
ouput.
</p>
</div>
<div class="subsection-level-extent" id="Why-a-personal-site-at-all-these-days_003f">
<h4 class="subsection"><span>Why a personal site at all these days?<a class="copiable-link" href="#Why-a-personal-site-at-all-these-days_003f"> &para;</a></span></h4>

<p>Having a personal website was the norm when the World Wide Web
exploded in the 90s: not necessarily a domain, but having a &ldquo;tilde&rdquo;
webpage somewhere was part of using the web
<a class="footnote" id="DOCF6" href="#FOOT6"><sup>6</sup></a>.
</p>
<p>While there were always attempts at building &ldquo;walled gardens&rdquo; (AOL
was one of the biggest, and in Portugal I still remember that there
was an attempt of making &ldquo;The Microsoft Network&rdquo; another one), what
ended up leading to the decrease in personal websites was the growth
in social networks, which made keeping a personal page less of a
requirement: the &ldquo;webpage&rdquo; became the &ldquo;profile page&rdquo;, often split
in many different networks.
</p>
<p>This was, at least, what happened to my online presence in the last
decade or so: instead of writing in my page, I started to write in
LinkedIn, or in Twitter, or using GitHub.
</p>
<p>I feel we are at a juncture where increased atomisation is fueled by
social networks, which are themselves fracturing and making it obvious
that there are real risks in making them our main internet
presence. This page is my attempt at reclaiming this presence, and by
doing so going back to some of the core principles of the world wide
web.
</p></div>
</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT6" href="#DOCF6">(6)</a></h5>
<p><a class="url" href="https://www.cameronsworld.net/">Cameron&rsquo;s World</a>
captures the richeness of this time, when personal web pages were
connected by &ldquo;web rings&rdquo; and each page had a certain <em class="emph">kitsch</em>
honesty to them, underneath all the animated images</p>
</div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
