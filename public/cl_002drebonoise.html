<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Prose content is https://creativecommons.org/licenses/by-sa/4.0/ (CC BY-SA 4.0).

Code mentioned usually links to a file with the licence information.

Copyright © 2013-2023 Frederico Muñoz -->
<title>Common Lisp terrain generator (interlaye.red)</title>

<meta name="description" content="Common Lisp terrain generator (interlaye.red)">
<meta name="keywords" content="Common Lisp terrain generator (interlaye.red)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="date" content="December 16, 2024">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concepts-index.html" rel="index" title="Concepts index">
<link href="index_toc.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Projects.html" rel="up" title="Projects">
<link href="tecra_005facpi.html" rel="next" title="tecra_acpi">
<link href="Emacs-Lisp-wmii.html" rel="prev" title="Emacs Lisp wmii">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span.program-in-footer {font-size: smaller}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="styles/brutex.css">

<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="rss.xml"><link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml"><link href="http://www.gesal.org/t.css?cl_002drebonoise.html" rel="stylesheet"><link rel="icon" type="image/x-icon" href="/images/favicon.png">
</head>

<body lang="en">
<div class="section-level-extent" id="cl_002drebonoise">
<div class="nav-panel">
<p>
Next: <a href="tecra_005facpi.html" accesskey="n" rel="next">Linux device driver for the Toshiba Tecra S1</a>, Previous: <a href="Emacs-Lisp-wmii.html" accesskey="p" rel="prev">Emacs Lisp wmii controller</a>, Up: <a href="Projects.html" accesskey="u" rel="up">Projects</a> &nbsp; [<a href="index_toc.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concepts-index.html" title="Index" rel="index">Index</a>][<a href="index.html">Top</a>] </p>
</div>
<hr>
<h3 class="section" id="Common-Lisp-terrain-generator"><span>Common Lisp terrain generator<a class="copiable-link" href="#Common-Lisp-terrain-generator"> &para;</a></span></h3>

<p><a class="url" href="https://github.com/fsmunoz/cl-rebonoise">cl-rebonoise</a> is a
Common Lisp implementation of the Simplex/Perlin noise approach
described in
<a class="url" href="https://www.redblobgames.com/maps/terrain-from-noise/">Amit
Patel&rsquo;s &ldquo;Making maps with noise functions&rdquo;</a>.
</p>
<div class="float">
<img class="image" src="images/cl-rebonoise.png" alt="images/cl-rebonoise">
<div class="caption"><p>Generated terrain.</p></div></div>
<p>Includes primitives and also helper functions, e.g.:
</p><div class="example user-lisp">
<pre class="example-preformatted"><div class="highlight" style="background: #f8f8f8"><pre style="line-height: 125%;"><span></span>(<span style="color: #19177C">grid-to-file</span><span style="color: #bbbbbb"> </span><span style="color: #666666">500</span><span style="color: #bbbbbb"> </span><span style="color: #666666">500</span><span style="color: #bbbbbb"> </span><span style="color: #BA2121">&quot;./examples/img/7-biome2.png&quot;</span>
<span style="color: #bbbbbb">              </span><span style="color: #19177C">:coefs</span><span style="color: #bbbbbb"> </span><span style="color: #666666">&#39;</span>((<span style="color: #666666">1</span><span style="color: #bbbbbb"> </span><span style="color: #666666">6</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #666666">2</span><span style="color: #bbbbbb"> </span><span style="color: #666666">11</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #666666">4</span><span style="color: #bbbbbb"> </span><span style="color: #666666">8</span>)<span style="color: #bbbbbb"> </span>(<span style="color: #666666">18</span><span style="color: #bbbbbb"> </span><span style="color: #666666">2</span>)<span style="color: #bbbbbb"> </span>)
<span style="color: #bbbbbb">              </span><span style="color: #19177C">:power</span><span style="color: #bbbbbb"> </span><span style="color: #666666">1.45</span>
<span style="color: #bbbbbb">              </span><span style="color: #19177C">:offset</span><span style="color: #bbbbbb"> </span><span style="color: #666666">166</span>
<span style="color: #bbbbbb">              </span><span style="color: #19177C">:biomes</span><span style="color: #bbbbbb"> </span><span style="color: #880000">t</span>)
</pre></div>
</pre></div>



<p>Amit&rsquo;s page has a wealth of resources on game-related topics, from an
implementation perspective, and I&rsquo;ve been following it for years.
</p>
<div class="subsection-level-extent" id="Why-I_0027m-interested">
<h4 class="subsection"><span>Why I&rsquo;m interested<a class="copiable-link" href="#Why-I_0027m-interested"> &para;</a></span></h4>

<p>Part of this is because one of my long-term projects is to recreate a
turn-based, play-by-(e)mail game that I was addicted to decades ago
<a class="footnote" id="DOCF8" href="#FOOT8"><sup>8</sup></a>, a project I keep coming
back to only to get derailed in building more &ldquo;infrastructure&rdquo;
instead of actual code: my commits to
<a class="url" href="https://github.com/hanshuebner/bknr-datastore">a Common
Lisp in-memory datastore</a> where part of this, and this library
another.
</p>
<p>While I end up getting side-tracked, one of this days I will piece
together all the side quests and make that game available. Well,
that&rsquo;s the plan, anyway.
</p>
</div>
</div>
<div class="footnotes-segment">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5 class="footnote-body-heading"><a id="FOOT8" href="#DOCF8">(8)</a></h5>
<p>It was very a local phenomenon called &ldquo;Idade das Trevas&rdquo;
(&ldquo;Dark Ages&rdquo;), and was very popular in the small-but-connected world
of strategy games enthusiasts in the 90s.</p>
</div>
<hr>
<p>
  <span class="program-in-footer">This document was generated on <em class="emph">December 16, 2024</em> using <a class="uref" href="https://www.gnu.org/software/texinfo/"><em class="emph">texi2any</em></a>.</span>
</p>


</body>
</html>
